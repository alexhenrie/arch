#!/bin/bash

#cp 31_hold_shift /etc/grub.d/
#if [ -z "`grep 'GRUB_FORCE_HIDDEN_MENU="true"' /etc/default/grub`" ]; then
#    echo GRUB_FORCE_HIDDEN_MENU=\"true\" >> /etc/default/grub
#fi
if [ -z "`grep 'GRUB_DISABLE_OS_PROBER="false"' /etc/default/grub`" ]; then
    echo GRUB_DISABLE_OS_PROBER=\"false\" >> /etc/default/grub
fi

mkdir -p /boot/grub
grub-mkconfig -o /boot/grub/grub.cfg
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB --compress=xz
