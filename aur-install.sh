#!/bin/bash -e

./yay-install.sh

add_config_line () {
    if [ ! -f $2 ]; then
        echo "$2 does not exist!"
    elif [ -z "`grep "$1" $2`" ]; then
        echo "$1" >> $2
    fi
}

yay -Syu --needed --noconfirm \
bcwc-pcie-git \
beignet \
flpsed \
hunspell-ca \
opencl-amd \
pdfjam-extras \
pithos \
pkgcacheclean \
qdirstat \
ttf-ms-fonts \
ventoy \
virtualbox-ext-oracle \
ycm-generator-git \
zramswap \

yay -Syu --needed --noconfirm --mflags=--nocheck\ CFLAGS=-w \
gst-plugins-ugly \
lib32-gst-libav

sudo sh -c 'echo "#!/bin/sh
pkgcacheclean" > /etc/cron.daily/pkgcacheclean.sh'
chmod +x /etc/cron.daily/pkgcacheclean.sh

touch ~/.nanorc
while read line; do
    add_config_line "$line" ~/.nanorc
done < /usr/share/nano-syntax-highlighting/nanorc.sample

sudo systemctl enable --now zramswap

#if [ ! -d ~/.config/btsync ]; then
#    sudo sed -i 's/0.0.0.0:8888/127.0.0.1:8888/' /etc/btsync.conf || {
#        echo 'Failed to lock down BitTorrent Sync'
#        exit 1
#    }
#    mkdir -p ~/.config/btsync
#    sudo cp ~/etc/btsync.conf ~/.config/btsync/btsync.conf
#    sudo chown $USER:$USER ~/.config/btsync/btsync.conf
#    mkdir -p ~/.btsync
#    sed -i "s/My Sync Device/`hostname`/" ~/.config/btsync/btsync.conf
#    sed -i "s/\\\/var\\\/lib\\\/btsync/\\\/home\\\/$USER\\\/.btsync/" ~/.config/btsync/btsync.conf
#    sed -i "s/\\\/var\\\/run\\\/btsync\\\/btsync.pid/\\\/home\\\/$USER\\\/.btsync\\\/btsync.pid/" ~/.config/btsync/btsync.conf
#fi

#systemctl --user enable btsync
#systemctl --user start btsync
