#!/bin/bash
# https://nixos.org/download/
sudo mkdir -m 0755 /nix
sudo chown $USER /nix
sh <(curl -L https://nixos.org/nix/install) --no-daemon # single-user installation
