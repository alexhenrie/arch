#!/bin/bash -e

install_package_file () {
    tar -xf $1.tar.gz
    cd $1
    makepkg
    sudo pacman -U --noconfirm *.pkg.tar*
    cd ..
}

AURTEMP=`mktemp -d`
pushd $AURTEMP > /dev/null
wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz
install_package_file yay
popd > /dev/null
rm -rf $AURTEMP
